<?php
require_once("Calculator.php");

class Chart {

    private $data = array();
    private $imageWidth;
    private $imageHeight;
    private $gridTop;
    private $gridLeft;
    private $gridBottom;
    private $gridRight;
    private $gridHeight;
    private $gridWidth;
    private $lineWidth;
    private $barWidth;
    private $font;
    private $fontSize;
    private $labelMargin;
    private $yMaxValue;
    private $backgroundColor;
    private $axisColor;
    private $labelColor;
    private $gridColor;
    private $barColor;

    public function __construct($data) {
        $this->data = $data;
    }

    public function updateValueByKey($key, $value) {
        $this->data[$key] = $value;
    }

    public function setyMaxValue($yMaxValue) {
        $this->yMaxValue = $yMaxValue;
    }

    public function saveImageToFile($fileName) {
        imagepng($this->chart, $fileName);
    }
    public function setChartSettings() {
        $this->setImageDimensions();
        $this->setGridDimensions();
        $this->setImagePlacement();
        $this->setLineWidths();
        $this->setFont();
        $this->setLabelMargin();
        $this->setGridLinesDistance();
    }

    public function drawChart() {
        $this->initImage();
        $this->setUpColors();
        $this->fillImage();
        $this->setImageThickness();
        $this->drawGridLines();
        $this->drawAxises();
        $this->drawBarsAndLabels();
    }

    /*
 * Chart settings and create image
 */

    // Image dimensions
    private function setImageDimensions() {
        $this->imageWidth = 510;
        $this->imageHeight = 280;
    }

    // Grid dimensions and placement within image
    private function setGridDimensions() {
        $this->gridTop = 10;
        $this->gridLeft = 40;
        $this->gridBottom = 240;
        $this->gridRight = 500;
    }

    private function setImagePlacement() {
        $this->gridHeight = $this->gridBottom - $this->gridTop;
        $this->gridWidth = $this->gridRight - $this->gridLeft;
    }
    // Bar and line width
    private function setLineWidths() {
        $this->lineWidth = 1;
        $this->barWidth = 20;
    }
    // Font settings
    private function setFont() {
        $this->font = realpath(".") . "\Cambria.ttf";
        $this->fontSize = 10;
    }
    // Margin between label and axis
    private function setLabelMargin() {
        $this->labelMargin = 8;
    }
      
    // Max value on y-axis
    private function setGridLinesDistance() {
        $this->yLabelSpan = ceil($this->yMaxValue/100) * 10;        
    }
    // Distance between grid lines on y-axis

    // Init image
    private function initImage() {
        $this->chart = imagecreate($this->imageWidth, $this->imageHeight);        
    }

    private function setUpColors() {
        $this->backgroundColor = imagecolorallocate($this->chart, 255, 255, 255);
        $this->axisColor = imagecolorallocate($this->chart, 85, 85, 85);
        $this->labelColor = imagecolorallocate($this->chart, 0, 0, 0);
        $this->gridColor = imagecolorallocate($this->chart, 212, 212, 212);
        $this->barColor = imagecolorallocate($this->chart, 47, 133, 217);
    }

    private function fillImage() {
        imagefill($this->chart, 0, 0, $this->backgroundColor);
    }

    private function setImageThickness() {
        imagesetthickness($this->chart, $this->lineWidth);
    }

    private function drawGridLines() {
        /*
        * Print grid lines bottom up
        */
        for($i = 0; $i <= $this->yMaxValue; $i += $this->yLabelSpan) {
            $y = $this->gridBottom - $i * $this->gridHeight / $this->yMaxValue;

            // draw the line
            imageline($this->chart, $this->gridLeft, $y, $this->gridRight, $y, $this->gridColor);

            // draw right aligned label
            $labelBox = imagettfbbox($this->fontSize, 0, $this->font, strval($i));
            $labelWidth = $labelBox[4] - $labelBox[0];

            $labelX = $this->gridLeft - $labelWidth - $this->labelMargin;
            $labelY = $y + $this->fontSize / 2;

            imagettftext($this->chart, $this->fontSize, 0, $labelX, $labelY, $this->labelColor, $this->font, strval($i));
        }
    }

    private function drawAxises() {
        /*
        * Draw x- and y-axis
        */
        imageline($this->chart, $this->gridLeft, $this->gridTop, $this->gridLeft, $this->gridBottom, $this->axisColor);
        imageline($this->chart, $this->gridLeft, $this->gridBottom, $this->gridRight, $this->gridBottom, $this->axisColor);
    }

    private function drawBarsAndLabels() {
        /*
        * Draw the bars with labels
        */

        $barSpacing = $this->gridWidth / count($this->data);
        $itemX = $this->gridLeft + $barSpacing / 2;

        foreach($this->data as $key => $value) {
            // Draw the bar
            $x1 = $itemX - $this->barWidth / 2;
            $y1 = $this->gridBottom - $value / $this->yMaxValue * $this->gridHeight;
            $x2 = $itemX + $this->barWidth / 2;
            $y2 = $this->gridBottom - 1;

            imagefilledrectangle($this->chart, $x1, $y1, $x2, $y2, $this->barColor);

            // Draw the label
            $labelBox = imagettfbbox($this->fontSize, 0, $this->font, $key);
            $labelWidth = $labelBox[4] - $labelBox[0];

            $labelX = $itemX - $labelWidth / 2;
            $labelY = $this->gridBottom + $this->labelMargin + $this->fontSize;

            imagettftext($this->chart, $this->fontSize, 0, $labelX, $labelY, $this->labelColor, $this->font, $key);
/*             $labelOffset = strlen($key) > 8 ? strlen($key) * 2.5 : strlen($key) * 1.2;
            
            imagettftext($this->chart, $this->fontSize, 0,$labelX+($labelOffset), $labelY+(15), 
                $this->labelColor, $this->font, $value); */

            $itemX += $barSpacing;
        }
    }
}
?>
