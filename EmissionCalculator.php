<?php

require_once("Calculator.php");

class EmissionCalculator extends Calculator {
/* 
   public function setVehicleType($vehicleType) {
     $this->vehicleType = $vehicleType;
    }

   public function setDistance($distance) {
       $this->distance = $distance;
    }  */

public function calculateAllEmissions() {
    $data = array(
        "Juna" => ceil($this->distance * TRAIN_MULTIPLY_FACTOR),
        "Bussi" => ceil($this->distance * BUS_MULTIPLY_FACTOR),
        "Sähköauto" => ceil($this->distance * ELECTRIC_CAR_MULTIPLY_FACTOR),
        "Auto" => ceil($this->distance * CAR_MULTIPLY_FACTOR),
        "Potkurikone" => ceil($this->distance * PROPELLER_PLANE_MULTIPLY_FACTOR),
        "Laiva" => ceil($this->distance * SHIP_MULTIPLY_FACTOR),
        "Suihkukone" => ceil($this->distance * JET_ENGINE_PLANE_MULTIPLY_FACTOR)
    );
    return $data;
}
}
?>