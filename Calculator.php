<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <title>Matka- ja päästölaskuri</title>
</head>

<body>
    <h1>Matka- ja päästölaskuri</h1>
    <h2>Yhteenveto</h2>

<?php

session_start();

define("TRAIN_MULTIPLY_FACTOR", 0.0023);
define("BUS_MULTIPLY_FACTOR", 0.0590);
define("ELECTRIC_CAR_MULTIPLY_FACTOR", 0.1228);
define("CAR_MULTIPLY_FACTOR", 0.2);
define("PROPELLER_PLANE_MULTIPLY_FACTOR", 0.2205);
define("SHIP_MULTIPLY_FACTOR", 0.3091);
define("JET_ENGINE_PLANE_MULTIPLY_FACTOR", 0.4705);
define("EMISSION_CHART_FILENAME", "emissionChart.png");

include_once("JourneyCalculator.php");
include_once("EmissionCalculator.php");
include_once("CarEmissionCalculator.php");
include_once("Chart.php");


 
class Calculator {
    protected $vehicleType;
    protected $distance;
    protected $persons;
    protected $fuelType;
    protected $speed;
    

    function __construct() {
            $this->vehicleType = $_SESSION["vehicleType"];
            $this->distance = $_SESSION["distance"];
            if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === "POST") {
                $this->persons = filter_input(
                INPUT_POST, "persons", FILTER_SANITIZE_NUMBER_INT);
            $this->fuelType = filter_input(
                INPUT_POST, "fuelType", FILTER_SANITIZE_STRING);    
            $this->speed = filter_input(
                INPUT_POST, "speed", FILTER_SANITIZE_NUMBER_INT);             
            
        }

    }



public function getVehicleType() { 
    return $this->vehicleType;

} 
public function getDistance() { 
    return $this->distance;

} 
public function getPersons() { 
    return $this->persons;

} 

public function getFuelType() {  
    return $this->fuelType;
}

public function getSpeed() { 
    return $this->speed;

} 
 
public function calculateTime() {
    $journeyCalculator = new JourneyCalculator();
     /* $journeyCalculator->setDistance($this->distance);
     $journeyCalculator->setSpeed($this->speed); */
    return $journeyCalculator->calculateTime();
   }
   public function calculateAllEmissions() {
     $emissionCalculator = new EmissionCalculator();
    /* $emissionCalculator->setVehicleType($this->vehicleType);
     $emissionCalculator->setDistance($this->distance); */
     return $emissionCalculator->calculateAllEmissions();
   }

   public function calculateCarEmission() {
       $carEmissionCalculator = new CarEmissionCalculator();
        /* $carEmissionCalculator->setPersons($this->persons);
        $carEmissionCalculator->setFuelType($this->fuelType);
        $carEmissionCalculator->setSpeed($this->speed); */
       return $carEmissionCalculator->calculateEmission();
   }
}
 
$calculator = new Calculator();
$data = $calculator->calculateAllEmissions();
$emission = $data[$calculator->getVehicleType()];

if ($calculator->getVehicleType() === "Auto") {
    $time = $calculator->calculateTime();
    $emission = $calculator->calculateCarEmission();

}
   echo "<p> Ajoneuvo: " . $calculator->getVehicleType() . "</p>";
   echo "<p> Matkan pituus: " . $calculator->getDistance() . " km</p>";

   if ($calculator->getVehicleType() === 'Auto') {
    echo "<p> Matkustajia: " . $calculator->getPersons() . "</p>";
    echo "<p> Polttoaine: " . $calculator->getFuelType() . "</p>";
    echo "<p> Arvioitu keskivauhti: " . $calculator->getSpeed() . " km/h</p>";
    echo "<p> Matkan kesto: <strong>" . $time . "</strong></p>";

   }
    echo "<p> Matkan hiilidioksidipäästöt: <strong>" . $emission . "kg</strong></p>";
    echo "<h3>Päästöt (kg) eri ajoneuvoille</h3>";


    $chart = new Chart($data);

    if ($calculator->getVehicleType() === "Auto") {
        $chart->updateValueByKey("Auto", $emission);
    }

    $chart->setYMaxValue(
        ($calculator->getDistance() * JET_ENGINE_PLANE_MULTIPLY_FACTOR +
        ($calculator->getDistance() * JET_ENGINE_PLANE_MULTIPLY_FACTOR) * 0.15));

    // Set ca settings and draw graphs

    $chart->setChartSettings();
    $chart->drawChart();
    $chart->saveImageToFile(EMISSION_CHART_FILENAME);
    echo "<p><img src='" . EMISSION_CHART_FILENAME . "' /></p>";
    echo "<a href='index.html'/>Etusivulle</a>";

?>
</body>
</html>